package webproj1;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Scanner;


public class CachingHTTPClient {
	static boolean debugFlag = false;
	static long lastAgeValue;
	
	public static void serveResponseFromCache(URLConnection connection, File file){
		System.out.printf("***** Serving from the cache – start *****\n");
		//@verify RESPONSE LINE PARSING
		System.out.printf("HTTP/1.1 200 OK\n"); 

		
		try {
			Scanner sc = new Scanner(file);
			while (sc.hasNextLine()) {
				//add age header
				System.out.println(sc.nextLine());
			}
			sc.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		System.out.printf("***** Serving from the cache – end *****\n");

	}
	
	public static void serveResponseFromOriginServer(URLConnection connection, PrintWriter pw) throws IOException {
		System.out.printf("***** Serving from the source – start *****\n");
		/* @todo ask about this
		Map<String,List<String>> requestProperties = connection.getRequestProperties();
		//if (debugFlag) 
		System.out.println(requestProperties); // this is null
		for(String s : requestProperties.keySet()) System.out.println(s); //output nothing
		//System.out.printf("%s %s %s\n", ); */
		String responseLine = "HTTP/1.1 200 OK\n";
		System.out.printf(responseLine);  //how do i parse the response's status line? since we are providing the response, do we hardcode this?
		pw.write(responseLine);
		
		if (debugFlag) {
			Map<String,List<String>> headerFields = connection.getHeaderFields();
			System.out.println(headerFields);
			System.out.printf("%s %s\n", "Age:", connection.getHeaderField("Age"));
			System.out.printf("%s %s\n", "Content-Encoding:", connection.getContentEncoding());
			System.out.printf("%s %s\n", "Content-Length:", connection.getContentLength());
			System.out.printf("%s %s\n", "Content-Type:", connection.getContentType());

			System.out.printf("%s %s\n", "Last-Modified:", connection.getHeaderField("Last-Modified"));
			System.out.printf("%s %s\n", "ETag:", connection.getHeaderField("ETag"));
			System.out.printf("%s %s\n", "Transfer-Encoding:", connection.getHeaderField("Transfer-Encoding"));
			System.out.printf("%s %s\n", "Transfer-Length:", connection.getHeaderField("Transfer-Length"));	
			System.out.printf("%s %s\n", "Connection:", connection.getHeaderField("Connection"));
		}
	

		

		String cacheControlValues = connection.getHeaderField("Cache-Control");
		System.out.printf("%s %s\n", "Cache-Control:", cacheControlValues);	
		pw.write("Cache-Control: " + cacheControlValues + "\n");

		String dateValueString = connection.getHeaderField("Date");
		System.out.printf("%s %s\n", "Date:", dateValueString);
		
		String expiresValue = connection.getHeaderField("Expires");
		System.out.printf("%s %s\n", "Expires:", expiresValue);
		pw.write("Expires: " + expiresValue + "\n");	
		
		long ageValueInSecs = getAgeValue(dateValueString);
		lastAgeValue = ageValueInSecs;
		pw.write("Age: " + ageValueInSecs + "\n");
		
		if( debugFlag ) System.out.println("ageValueInSecs = " + ageValueInSecs);

		printAndCacheHTML(connection, pw);
		pw.flush();
		pw.close();
		System.out.printf("\n***** Serving from the source – end *****\n");

	}
	
	public static void printAndCacheHTML(URLConnection connection, PrintWriter pw) throws IOException
	{
		InputStream input = connection.getInputStream();
		byte[] buffer = new byte[4096];
		int n = -1;
		
		while ( (n = input.read(buffer)) != -1 ) {
			if (n > 0) {
				System.out.write(buffer, 0, n);
				for(int i = 0; i < n; i++){
					pw.print((char) buffer[i]);
				}
			}
			
		}
	}
	public static long getAgeValue(String dateValueString){
		LocalDateTime nowTimePoint = LocalDateTime.now();
		if (debugFlag) System.out.printf("LocalDate.now() = %s\n", nowTimePoint);
		
		DateTimeFormatter formatter = DateTimeFormatter.RFC_1123_DATE_TIME;
		LocalDateTime dateTimePoint = LocalDateTime.parse(dateValueString, formatter);
		if (debugFlag) System.out.printf("LocalDate.parse(dateValueString, formatter) = %s\n", dateTimePoint);
		Duration ageDuration = Duration.between(nowTimePoint, dateTimePoint); //present - past @confirm
		long ageValueInSecs = ageDuration.getSeconds();
		if (debugFlag) System.out.printf("ageValueInSecs = %d\n", ageValueInSecs);
		return ageValueInSecs;
	}
	
	public static int parseMaxAgeValue(String cc, String nameMappedToValue) {
		String[] cacheControlContent = cc.split(",");
		String[] maxAgeNameValuePair = null;
		int maxAgeValueInSec = 0;
		for (String s : cacheControlContent) {
			if (debugFlag) System.out.println(s);
			if (s.contains(nameMappedToValue)) {
				maxAgeNameValuePair = s.split("=");
				maxAgeValueInSec = Integer.parseInt(maxAgeNameValuePair[1]);
				if(debugFlag) System.out.println(maxAgeValueInSec);
			}	
		}
		return maxAgeValueInSec;
	}

	public static void checkClientArguments(String[] args){
		if (args.length != 1) {
			throw new RuntimeException("Usage: \n" +
					"java CachingHTTPClient <URL>\n");
		}
	}
	
	public static URL generateUrlObject(String uri){
		URL url = null;
		
		try {
			url = new URL(uri);
			if (debugFlag)
				System.out.printf("%s %s\n","Url: ", url);
		} catch(MalformedURLException e) {
			e.printStackTrace();
		}
		assert url != null;
		return url;
	}
	
	public static void main(String[] args) {


		try {
			checkClientArguments(args);
			String uri = args[0];
			URL url = generateUrlObject(uri);
			String cacheDirName = "/tmp/gsg299/assignment1/";
			File cacheDir = new File(cacheDirName);
			
			if (debugFlag) System.out.println("cacheDir is a dir: " + cacheDir.isDirectory());
			
			String cachedUriName = cacheDirName + url.getHost();
			File cachedUri = new File(cachedUriName);
			if (debugFlag) System.out.println("cachedRes exists: " + cachedUri.exists());

			URLConnection connection = url.openConnection();

			if (cachedUri.exists()){
				//serveResponseFromCache(connection, cachedUri);

				String cacheControl = connection.getHeaderField("Cache-Control");
				boolean isCachePrivate = false;
				boolean isSMaxAgePresent = false;
				boolean isMaxAgePresent = false;
				if(cacheControl != null){
					isCachePrivate = cacheControl.contains("private");
					isSMaxAgePresent = cacheControl.contains("s-max-age");
					isMaxAgePresent = cacheControl.contains("max-age");
				}
				long expires = connection.getExpiration() / 1000; //now in seconds
				boolean isExpiresHeaderPresent = expires > 0;
				long lastModified = connection.getLastModified() / 1000;
				boolean isLastModifiedPresent = lastModified > 0;
				
				if (debugFlag) {
					System.out.println(cacheControl);
					System.out.println("isCachePrivate " + isCachePrivate);
					System.out.println("isSMaxAgePresent " + isSMaxAgePresent);
					System.out.println("isMaxAgePresent " + isMaxAgePresent);
					System.out.println("expires in seconds = " + expires);
					System.out.println("isExpiresHeaderPresent " + isExpiresHeaderPresent);
					System.out.println("lastModified in seconds = " + lastModified);
					System.out.println("isLastModifiedPresent " + isLastModifiedPresent);
					System.out.println("lastAgeValue " + lastAgeValue);
				}
				
				// Main Control Flow
				if (isCachePrivate) {
					PrintWriter pw = new PrintWriter(cachedUri);
					serveResponseFromOriginServer(connection, pw);
				}
				else if (isSMaxAgePresent) {
					int sMaxAgeValue = parseMaxAgeValue(cacheControl, "s-max-age"); 
					if (lastAgeValue < sMaxAgeValue)
						serveResponseFromCache(connection, cachedUri);
					else {
						cachedUri.delete();
						PrintWriter pw = new PrintWriter(cachedUri);
						serveResponseFromOriginServer(connection, pw);
					}
				}
				else if (isMaxAgePresent) {
					int maxAgeValue = parseMaxAgeValue(cacheControl, "max-age");
					if (lastAgeValue <= maxAgeValue)
						serveResponseFromCache(connection, cachedUri);
					else {
						cachedUri.delete();
						PrintWriter pw = new PrintWriter(cachedUri);
						serveResponseFromOriginServer(connection, pw);
					}		
				}
				else if (isExpiresHeaderPresent) {
					if(lastAgeValue > expires) {
						cachedUri.delete();
						PrintWriter pw = new PrintWriter(cachedUri);
						serveResponseFromOriginServer(connection, pw);
					}
					else {
						serveResponseFromCache(connection, cachedUri);
					}
				}
				else if (isLastModifiedPresent) { //heuristic Last-Modified-Time
					int arbitraryTimeLimit = 60*60*24; //currently 1 day
					if (lastModified > arbitraryTimeLimit) {
						cachedUri.delete();
						PrintWriter pw = new PrintWriter(cachedUri);
						serveResponseFromOriginServer(connection, pw);
					}
					else {
						serveResponseFromCache(connection, cachedUri);
					}	
				}	
				else {
					cachedUri.delete();
					PrintWriter pw = new PrintWriter(cachedUri);
					serveResponseFromOriginServer(connection, pw);
				}
			}
			else {
				cachedUri.createNewFile();
				PrintWriter pw = new PrintWriter(cachedUri);
				serveResponseFromOriginServer(connection, pw);
				
				pw.flush();
				pw.close();			
			}
			
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
